var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query =
        'SELECT g.*, ROUND(AVG(r.rating),1) AS avg_rating ' +
        'FROM Game g ' +
        'LEFT JOIN Review r ON r.game_id = g.game_id ' +
        'GROUP BY g.game_id ' +
        'ORDER BY game_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

