var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query =
        'SELECT g.game_id, g.game_name, YEAR(g.release_date) as release_year, g.multiplayer, ROUND(AVG(r.rating),1) AS avg_rating ' +
        'FROM Game g ' +
        'LEFT JOIN Review r ON r.game_id = g.game_id ' +
        'GROUP BY g.game_id ' +
        'ORDER BY game_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(game_id, callback) {
    var query =
        'SELECT g.*, vgp.publisher AS publisher, vgd.developer AS developer, a.account_name, ag.hours_played, r.rating, r.feedback, r.review_date ' +
        'FROM Game g ' +
        'JOIN vGame_Developer vgd ON vgd.game_id = g.game_id ' +
        'JOIN vGame_Publisher vgp ON vgp.game_id = g.game_id ' +
        'JOIN Account_Game ag ON ag.game_id = g.game_id ' +
        'JOIN Account a ON a.account_id = ag.account_id ' +
        'JOIN Review r ON r.game_id = g.game_id AND r.account_id = a.account_id ' +
        'WHERE g.game_id = ? ' +
        'ORDER BY g.game_name, r.review_date DESC;';
    var queryData = [game_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

