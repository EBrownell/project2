var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM Tag ORDER BY tag_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(tag_id, callback) {
    var query =
        'SELECT t.*, g.*, ROUND(AVG(r.rating),1) AS avg_rating ' +
        'FROM Tag t ' +
        'LEFT JOIN Game_Tag tg ON tg.tag_id = t.tag_id ' +
        'LEFT JOIN Game g ON g.game_id = tg.game_id ' +
        'LEFT JOIN Review r ON r.game_id = g.game_id ' +
        'WHERE t.tag_id = ? ' +
        'GROUP BY g.game_name ' +
        'ORDER BY t.tag_name, game_name;';
    var queryData = [tag_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

