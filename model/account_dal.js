var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query =
        'SELECT a.*, fn_account_get_num_of_games(a.account_id) AS num_of_games ' +
        'FROM Account a ' +
        'ORDER BY a.account_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query =
        'SELECT a.*, g.game_id, game_name, ag.hours_played ' +
        'FROM Account a ' +
        'JOIN Account_Game ag ON ag.account_id = a.account_id ' +
        'JOIN Game g ON g.game_id = ag.game_id ' +
        'WHERE a.account_id = ? ' +
        'ORDER BY game_name;';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE ACCOUNT
    var query = 'INSERT INTO Account (account_name, email) VALUES (?,?)';

    var queryData = [params.account_name, params.email];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE ACCOUNT_ID RETURNED AS insertId AND THE SELECTED GAME_IDs INTO ACCOUNT_GAME
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO Account_Game (account_id, game_id, hours_played) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountGameData = [];
        if (params.game_id.constructor === Array) {
            for (var i = 0; i < params.game_id.length; i++) {
                accountGameData.push([account_id, params.game_id[i], 0]);
            }
        }
        else {
            accountGameData.push([account_id, params.game_id, 0]);
        }

        // NOTE THE EXTRA [] AROUND accountGameData
        connection.query(query, [accountGameData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(account_id, callback) {
    var query =
        'DELETE FROM Account WHERE account_id = ?;';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

























