var express = require('express');
var router = express.Router();
var game_dal = require('../model/game_dal');

// View All games
router.get('/all', function(req, res) {
    game_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('game/gameViewAll', { 'result':result });
        }
    });

});

// View the game for the given id
router.get('/', function(req, res){
    if(req.query.game_id == null) {
        res.send('game_id is null');
    }
    else {
        game_dal.getById(req.query.game_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('game/gameViewById', {'result': result});
            }
        });
    }
});

module.exports = router;
