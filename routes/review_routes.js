var express = require('express');
var router = express.Router();
var review_dal = require('../model/review_dal');

// View All games (to select to view reviews)
router.get('/all', function(req, res) {
    review_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('review/reviewViewAll', { 'result':result });
        }
    });

});

module.exports = router;
