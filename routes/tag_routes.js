var express = require('express');
var router = express.Router();
var tag_dal = require('../model/tag_dal');

// View All accounts
router.get('/all', function(req, res) {
    tag_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('tag/tagViewAll', { 'result':result });
        }
    });

});

// View the Tag for the given id
router.get('/', function(req, res){
    if(req.query.tag_id == null) {
        res.send('tag_id is null');
    }
    else {
        tag_dal.getById(req.query.tag_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('tag/tagViewById', {'result': result});
            }
        });
    }
});

module.exports = router;
